﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace ClickSoftwareTest
{
    internal class TestSelenium
    {
        private static void Main(string[] args)
        {
            // Initialize the Chrome Driver
            using (var driver = new ChromeDriver())
            {
                //Navigate to page
                driver.Navigate().GoToUrl("https://login.salesforce.com/");

                driver.Manage().Window.Maximize();

                // Save elements as variables 
                var userNameField = driver.FindElementById("username");
                var userPasswordField = driver.FindElementById("password");
                var loginButton = driver.FindElementById("Login");

                // Type user and password
                userNameField.SendKeys("bestia2001arroba@gmail.com");
                userPasswordField.SendKeys("whatever123");

                // Click login button
                loginButton.Click();

                //Wait for validation code to be send to email

                // Calendar variables 
                //var calendar = driver.FindElementById("JumpToDate");
                var clickRight =
                    new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(
                        ExpectedConditions.ElementExists(
                            By.XPath(
                                "//*[@id='GanttContainer']/div[1]/div[1]/div[4]")));


                // Navigate to Date 
                do
                {
                    clickRight.Click();
                } while (!driver.FindElementByXPath("//*[@id='GanttContainer']/div[1]/div[1]")
                    .Text.Equals("Today\r\n19 Dec 2016\r\nDaily"));

                //Right click on Appointment
                var appoinment =
                    driver.FindElementByXPath(
                        "//*[@id='GanttContainer']/div[1]/div[3]/table/tbody/tr[7]/td[2]/div/div[8]/div");


                var action = new Actions(driver).ContextClick(appoinment);
                action.Build().Perform();

                //Right clik on Reschedule 
                var reschedule = driver.FindElements(By.XPath("//*[contains(@id,'_reschedule')]"));
                reschedule[1].Click();

                //Verify the new appointment has moved to the right place

                var verify =
                    new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(
                        ExpectedConditions.ElementExists(
                            By.XPath(
                                "//*[@id='GanttContainer']/div[1]/div[3]/table/tbody/tr[3]/td[2]/div/div[8]/div/div")))
                        .Displayed;


                if (verify.Equals(true))

                {
                    Console.WriteLine("Succes!,Appointment successfully schedule");
                }
                else
                {
                    throw new Exception("Fail,there's been an error with rescheduling");
                }
            }
        }
    }
}